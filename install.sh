#!/bin/bash
set -e
VERSION="$1"
URL=$(php get-url.php "$VERSION")
cd working
wget $URL -O "php-$VERSION.tar.gz"
tar xzf "php-$VERSION.tar.gz"
mkdir "../versions/$VERSION"
cd "php-$VERSION"
./configure --prefix=../../versions/$VERSION
make
make install
../../versions/$VERSION/bin/php --version

