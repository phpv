#!/bin/bash
set -e
VERSION="$1"
BIN="versions/$VERSION/bin"
mv "$BIN/php" "$BIN/php-cgi"
mv "working/php-$VERSION/sapi/cli/php" "$BIN/php"
versions/$VERSION/bin/php --version
