#!/bin/bash
set -e
cd "working/php-$1-dev"
cvs up
./config.nice
make
make install
