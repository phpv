#!/usr/bin/env php
<?php

function extract_links($url) {
    $doc = new DOMDocument();
    @$doc->loadHTMLFile($url);
    $xpath = new DOMXPath($doc);
    return $xpath->query('//a');
}

if (!isset($_SERVER['argv'][1])) {
    echo <<<EOF
Usage: php get-url.php version

EOF;
    exit(1);
}
$install_version = $_SERVER['argv'][1];

$url_mirror = 'http://us2.php.net';
$url_releases = $url_mirror . '/releases/';
$url_downloads = $url_mirror . '/downloads.php';
$url_qa = 'http://qa.php.net';

$link_sets = array(
    extract_links($url_releases),
    extract_links($url_downloads),
    extract_links($url_qa)
);
$index = array();

foreach ($link_sets as $links) {
    foreach ($links as $link) {
        $url = $link->getAttribute('href');
        if (!$url) continue;
        $result = preg_match('#/php-([^-]+?)\.tar\.gz#', $url, $matches);
        if (!$result) continue;
        list($full, $version) = $matches;
        $url = str_replace('/from/a/mirror', '/from/this/mirror', $url); // make download ready
        if ($url[0] == '/') $url = $url_mirror . $url; // heuristic assumes absolute paths are used
        $index[$version] = $url;
    }
}

// maybe add qa support

if (!isset($index[$install_version])) {
    fwrite(STDERR, "Could not find $install_version\n");
    exit(1);
}

echo $index[$install_version];

